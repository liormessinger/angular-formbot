import { Component, OnInit } from '@angular/core';
import { ServoService } from '../servo.service';

import { MessageModel } from '../message-model';
@Component({
  selector: 'app-state-intro',
  templateUrl: './state-intro.component.html',
  styleUrls: ['./state-intro.component.scss']
})
export class StateIntroComponent implements OnInit {
  visitReason = [{ reasonId: "step1", text: "Go to step 1" }, { reasonId: "step2", text: "Wire Transfer" }];
  selectedValue: any;
  constructor(private servoService: ServoService) {

  }
  submitted = false;

  onSubmit() {
    this.submitted = true;
    let msg = new MessageModel();
    msg.originRoute = 'intro';
    msg.intentId = this.selectedValue.reasonId;
    msg.addEntity({ name: 'visitReason', value: this.selectedValue.text });
    this.servoService.submit(msg);
  }

  ngOnInit() {
  }

}
