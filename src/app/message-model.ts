export interface Entity {
    name: string;
    value: string;
    score?: number;
}
export class MessageModel {

    _intentId: string;
    entities: any;
    userId: string;
    originRoute: string;
    constructor() {
        this.entities = {};
        this.intentId = '';
    }

    addEntity(entity: Entity) {
        this.entities[entity.name] = this.entities[entity.name] || [];
        this.entities[entity.name].push(entity.value);
    }
    set intentId(v) {
        this._intentId = v;
    }

    get intentId() {
        return this._intentId;
    }

};
