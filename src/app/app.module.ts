import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StateIntroComponent } from './state-intro/state-intro.component';
import { StateStep1Component } from './state-step1/state-step1.component';
import { StateStep2Component } from './state-step2/state-step2.component';
import { ServoService } from './servo.service';
import { HttpModule } from '@angular/http';
import { StateStartComponent } from './state-start/state-start.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    StateIntroComponent,
    StateStep1Component,
    StateStep2Component,
    StateStartComponent
  ],
  imports: [
    BrowserModule, FormsModule,
    AppRoutingModule, HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
