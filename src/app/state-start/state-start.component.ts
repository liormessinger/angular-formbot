import { Component, OnInit } from '@angular/core';
import { ServoService } from '../servo.service';
import { MessageModel } from '../message-model';
@Component({
  selector: 'app-state-start',
  templateUrl: './state-start.component.html',
  styleUrls: ['./state-start.component.scss']
})
export class StateStartComponent implements OnInit {
  constructor(private servoService: ServoService) {

  }
  submitted = false;

  onSubmit() {
    this.submitted = true;
    let msg = new MessageModel();
    msg.intentId = 'start';
    msg.originRoute = 'start';
    this.servoService.submit(msg);
  }
  ngOnInit() {
  }

}
