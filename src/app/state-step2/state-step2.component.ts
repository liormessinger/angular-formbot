import { Component, OnInit } from '@angular/core';
import { ServoService } from '../servo.service';
import { MessageModel } from '../message-model';

@Component({
  selector: 'app-state-step2',
  templateUrl: './state-step2.component.html',
  styleUrls: ['./state-step2.component.scss']
})
export class StateStep2Component implements OnInit {
  balance: string;
  wireAmount: string;
  constructor(private servoService: ServoService) {
    this.balance = servoService.payload.balance;
  }

  ngOnInit() {
  }

  submitted = false;

  onSubmit() {
    this.submitted = true;
    let msg = new MessageModel();
    msg.originRoute = 'step2';
    msg.addEntity({ name: 'wireAmount', value: this.balance });
    this.servoService.submit(msg);
  }

}
