import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StateIntroComponent } from './state-intro/state-intro.component';
import { StateStep1Component } from './state-step1/state-step1.component';
import { StateStep2Component } from './state-step2/state-step2.component';
import { StateStartComponent } from './state-start/state-start.component';

const routes: Routes = [{
  path: 'intro',
  component: StateIntroComponent
}, {
  path: 'step1',
  component: StateStep1Component
}, {
  path: 'step2',
  component: StateStep2Component
}, {
  path: 'start',
  component: StateStartComponent
}, {
  path: '',
  redirectTo: '/start',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
